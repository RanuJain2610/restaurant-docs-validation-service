package ocr

//Ocr Response
type OcrResponse struct {
	DocumentType         string `json:"doc_type"`
	FssaiLicenceNumber   string `json:"registration_num"`
	Address              string `json:"premises_address"`
	FssaiLicenceValidity string `json:"expiry_date"` //license validity returned as DD/MM/YYYYs
}
