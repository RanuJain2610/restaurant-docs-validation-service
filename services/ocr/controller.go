package ocr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetOCRResponse(docURL string) (*OcrResponse, error) {

	fmt.Println("Calling API...")
	client := &http.Client{}
	req, err := http.NewRequest("POST", "http://ocr-kv.production.singapore/fssai", strings.NewReader("file="+docURL))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var responseObject OcrResponse
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("OCR API Response as struct %+v\n", responseObject)
	return &responseObject, nil
}
