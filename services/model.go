package services

type StatusCode string

const (
	STATUS_SUCCESS StatusCode = "SUCCESS"
	STATUS_FAILURE StatusCode = "FAILURE"
)

//Response response data structure
type Response struct {
	Status int         `json:"status"`
	Code   StatusCode  `json:"code"`
	Data   interface{} `json:"data"`
}

//ErrorResponse Error response data structure
type ErrorResponse struct {
	ErrorCode    string `json:"errorCode"`
	ErrorMessage string `json:"errorMessage"`
}
