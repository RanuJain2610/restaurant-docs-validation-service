package fssai

import (
	"net/http"

	"bitbucket.org/swigy/restaurant-docs-validation-service/services"
	"github.com/gorilla/mux"
)

func Handlers(r *mux.Router) {

	// /restaurant-fssai/status
	r.HandleFunc("/restaurant-fssai/status", getFssaitatusHandler).Methods(http.MethodGet, http.MethodOptions)

	// /restaurant-fssai/doc/data

	// /restaurant-fssai/ack/data

}

func getFssaitatusHandler(w http.ResponseWriter, r *http.Request) {
	var request FssaiStatusRequest
	err := services.ParseRequest(r, &request)

	if err != nil {
		//To Do log the error
		errResp := services.ErrorResponse{
			ErrorCode:    "Bad_Request",
			ErrorMessage: "bad request",
		}
		services.ReturnResponse(w, errResp, http.StatusBadRequest)
		return
	}

	fssaiStatus := getFssaiStatus(request)
	resp := services.Response{
		Status: 1,
		Code:   services.STATUS_SUCCESS,
		Data:   fssaiStatus,
	}

	services.ReturnResponse(w, resp, http.StatusOK)
}
