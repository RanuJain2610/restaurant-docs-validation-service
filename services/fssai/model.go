package fssai

type ReasonCode string

const (
	VALID_ADDRESS          ReasonCode = "VALID_ADDRESS"
	APPROVED_WITH_INPUT    ReasonCode = "APPROVED_WITH_INPUT"
	APPROVED_WITH_OCR      ReasonCode = "APPROVED_WITH_OCR"
	ADDRESS_MISMATCH       ReasonCode = "ADDRESS_MISMATCH"
	INACTIVE_FSSAI_LICENSE ReasonCode = "INACTIVE_FSSAI_LICENSE"
	DATA_MISMATCH          ReasonCode = "DATA_MISMATCH"
	FOSCOS_FAILED          ReasonCode = "FOSCOS_FAILED"
	OCR_FAILED             ReasonCode = "OCR_FAILED"
)

type FssaiStatus string

const (
	APPROVED FssaiStatus = "APPROVED"
	REJECTED FssaiStatus = "REJECTED"
)

//FssaiStatusRequest -
type FssaiStatusRequest struct {
	RestaurantId       int64  `json:"restaurant_id"`
	FssaiDocumentUrl   string `json:"fssai_document_url"`
	FssaiLicenceNumber string `json:"fssai_licence_number"`
	RestaurantAddress  string `json:"resaurant_address"`
}

//FssaiStatusResponse -
type FssaiStatusResponse struct {
	RestaurantId          int64       `json:"restaurant_id"`
	FssaiLicenceNumber    string      `json:"fssai_licence_number"`
	RestaurantAddressData string      `json:"resaurant_address"`
	FssaiLicenceValidity  string      `json:"fssai_licence_validity"`
	ValidationDate        int64       `json:"validation_date"` //license validity returned as unix epoch
	FssaiStatus           FssaiStatus `json:"fssai_status"`
	ReasonCode            ReasonCode  `json:"reason_code"`
}
