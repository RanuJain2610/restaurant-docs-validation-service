package fssai

import (
	"time"

	fuzzy "github.com/paul-mannino/go-fuzzywuzzy"

	"bitbucket.org/swigy/restaurant-docs-validation-service/services/foscos"
	"bitbucket.org/swigy/restaurant-docs-validation-service/services/ocr"
)

func getFssaiStatus(request FssaiStatusRequest) *FssaiStatusResponse {

	var reasonCode ReasonCode
	var fssaiStatus FssaiStatus
	var licenseNumber string
	var addreess string
	var expiryDate string

	ocrResponse, ocrErr := ocr.GetOCRResponse(request.FssaiDocumentUrl)

	if ocrErr == nil {
		if request.FssaiLicenceNumber == ocrResponse.FssaiLicenceNumber {
			licenseNumber = request.FssaiLicenceNumber
			foscosDocResponse, err := foscos.GetFoscosResponse(licenseNumber)
			if err == nil {
				if foscosDocResponse.IsLicenseActive {
					if validateAddress(ocrResponse.Address, request.RestaurantAddress) {
						if validateAddress(foscosDocResponse.Address, ocrResponse.Address) {
							addreess = ocrResponse.Address
							reasonCode = VALID_ADDRESS
							fssaiStatus = APPROVED
						} else {
							reasonCode = ADDRESS_MISMATCH
							fssaiStatus = REJECTED
						}
					} else {
						if validateAddress(foscosDocResponse.Address, request.RestaurantAddress) {
							addreess = foscosDocResponse.Address
							reasonCode = APPROVED_WITH_INPUT
							fssaiStatus = APPROVED
						} else if validateAddress(foscosDocResponse.Address, ocrResponse.Address) {
							addreess = ocrResponse.Address
							reasonCode = APPROVED_WITH_OCR
							fssaiStatus = APPROVED
						} else {
							reasonCode = ADDRESS_MISMATCH
							fssaiStatus = REJECTED
						}
					}
				} else {
					reasonCode = INACTIVE_FSSAI_LICENSE
					fssaiStatus = REJECTED
				}
			} else {
				reasonCode = FOSCOS_FAILED
				fssaiStatus = REJECTED
			}
		} else if ocrResponse.FssaiLicenceNumber != "" {
			licenseNumber = ocrResponse.FssaiLicenceNumber
			foscosDocResponse, err := foscos.GetFoscosResponse(ocrResponse.FssaiLicenceNumber)
			if err == nil {
				if foscosDocResponse.IsLicenseActive {
					if validateAddress(foscosDocResponse.Address, ocrResponse.Address) {
						addreess = ocrResponse.Address
						reasonCode = APPROVED_WITH_OCR
						fssaiStatus = APPROVED
					} else {
						reasonCode = ADDRESS_MISMATCH
						fssaiStatus = REJECTED
					}
				} else {
					reasonCode = INACTIVE_FSSAI_LICENSE
					fssaiStatus = REJECTED
				}
			} else {
				reasonCode = FOSCOS_FAILED
				fssaiStatus = REJECTED
			}
		} else if request.FssaiLicenceNumber != "" {
			licenseNumber = request.FssaiLicenceNumber
			foscosDocResponse, err := foscos.GetFoscosResponse(request.FssaiLicenceNumber)
			if err == nil {
				if foscosDocResponse.IsLicenseActive {
					if validateAddress(foscosDocResponse.Address, request.RestaurantAddress) {
						addreess = foscosDocResponse.Address
						reasonCode = APPROVED_WITH_INPUT
						fssaiStatus = APPROVED
					} else {
						reasonCode = ADDRESS_MISMATCH
						fssaiStatus = REJECTED
					}
				} else {
					reasonCode = INACTIVE_FSSAI_LICENSE
					fssaiStatus = REJECTED
				}
			} else {
				reasonCode = FOSCOS_FAILED
				fssaiStatus = REJECTED
			}
		} else {
			reasonCode = DATA_MISMATCH
			fssaiStatus = REJECTED
		}
		if ocrResponse.FssaiLicenceValidity != "" {
			expiryDate = ocrResponse.FssaiLicenceValidity
		}
	} else {
		reasonCode = OCR_FAILED
		fssaiStatus = REJECTED
	}

	return &FssaiStatusResponse{
		RestaurantId:          request.RestaurantId,
		FssaiLicenceNumber:    licenseNumber,
		RestaurantAddressData: addreess,
		FssaiLicenceValidity:  expiryDate,
		ValidationDate:        time.Now().Unix(),
		FssaiStatus:           fssaiStatus,
		ReasonCode:            reasonCode,
	}
}

func validateAddress(mainAddress string, validatedAddress string) bool {
	return fuzzy.PartialRatio(mainAddress, validatedAddress) >= 80
}
