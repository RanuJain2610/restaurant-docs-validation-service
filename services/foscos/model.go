package foscos

//Foscos Doc Response
type FoscosDocResponse struct {
	FboId              int64  `json:"fboid"`
	FssaiLicenceNumber string `json:"licenseno"`
	IsLicenseActive    bool   `json:"licenseactiveflag"`
	ACKNumber          string `json:"displayrefid"`
	CompanyName        string `json:"companyname"`
	Address            string `json:"premiseaddress"`
	State              string `json:"statename"`
	PinCode            int64  `json:"premisepincode"`
}
