package foscos

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func GetFoscosResponse(LicenseNo string) (*FoscosDocResponse, error) {
	fmt.Println("Calling API...")
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://heoaej3633.execute-api.ap-southeast-1.amazonaws.com/fbo-search?licenseno="+LicenseNo, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var responseObject FoscosDocResponse
	json.Unmarshal(bodyBytes, &responseObject)
	fmt.Printf("Foscos API Response as struct %+v\n", responseObject)
	return &responseObject, nil
}
