package services

import (
	"encoding/json"
	"net/http"
)

//ParseRequest reads the request body and creates a
func ParseRequest(r *http.Request, target interface{}) error {
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(target)
}

//ReturnResponse writes the response into the http ResponseWriter after marshalling the response into json
func ReturnResponse(w http.ResponseWriter, response interface{}, httpStatus int) {

	js, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	w.Write(js)
}
