module bitbucket.org/swigy/restaurant-docs-validation-service

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/paul-mannino/go-fuzzywuzzy v0.0.0-20200127021948-54652b135d0e
)
