package main

import (
	"fmt"
	"log"
	"net/http"

	fssai_service "bitbucket.org/swigy/restaurant-docs-validation-service/services/fssai"
	"github.com/gorilla/mux"
)

func health(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "ok")
}

func handleRequests() {
	router := mux.NewRouter().StrictSlash(true)
	router.Use(mux.CORSMethodMiddleware(router))

	//Health Check
	router.HandleFunc("/health", health)

	//Handle Fssai requests
	fssai_service.Handlers(router)

	log.Fatal(http.ListenAndServe(":8082", router))
}

func main() {
	handleRequests()
}
